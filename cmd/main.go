package main

import (
	"fmt"
	"strings"

	"github.com/tkuchiki/go-timezone"
)

func removeDuplicateStr(strSlice []string) []string {
	allKeys := make(map[string]bool)
	list := []string{}
	for _, item := range strSlice {
		if _, value := allKeys[item]; !value {
			allKeys[item] = true
			list = append(list, item)
		}
	}
	return list
}

func keysByValue(m map[string][]string, value string) []string {
	var keys []string
	for k, v := range m {
		// if value == v {
		// 	keys = append(keys, k)
		// }
		for _, vv := range v {
			if strings.Contains(vv, value) {
				keys = append(keys, k)
			}
		}
	}
	return removeDuplicateStr(keys)
}

func main() {
	tz := timezone.New().Timezones()
	getKeys := keysByValue(tz, "Europe")
	fmt.Println(getKeys)
}
